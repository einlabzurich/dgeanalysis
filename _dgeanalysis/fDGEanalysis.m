function [] = fCESTanalysis(conf)
% Clear workspace and classes
clearvars -except conf 
close all

DEBUG_SAVE = 0;
DEBUG_LOAD = 0;
DEBUG_REIMPORT_CONF = 0;
DEBUG_FILE = 'F:\Afroditi\MatlabCode\CEST\CESTanalysis_v03\_CESTanalysis\Example\20210105_124942_CEST_Red_1_4\Red0501.mat';

% Import the configuration options
if nargin < 1
    conf = CESTanalysisConf();
end

if conf.disableWarnings == 1
    warning('off')
end

% Import the experiments parameters as cell array of exp structures
exps = readExpsFromXLScest(conf.tableFileName, conf.inFilesBasePath);

% Run the analysis for each experiment in exps cell array
nExps = length(exps);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  LOOP FOR N-th EXPERIMENT  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for e = 1:nExps   
    tic  
    if DEBUG_LOAD == 1
        disp('Loading preanalyzed data for DEBUGGING...');
        load(DEBUG_FILE);
%         if DEBUG_REIMPORT_CONF == 1
            disp('Reimporting configuration...');
            conf = CESTanalysisConfig();
%         end
    else
        %% load CEST data
        if exist([fullfile(exps{e}.workdir, exps{e}.shortName),'.mat']) == 2
            load([fullfile(exps{e}.workdir, exps{e}.shortName),'.mat']); % loading the data
            disp('Loading pre-existing Matlab file with data.')
        elseif exist([fullfile(exps{e}.workdir, exps{e}.shortName),'.mat']) == 0
            [dynamic_scan, initial_z_spectrum, final_z_spectrum, anatomical_scan] = loadCEST(e,exps);
        end       

        %% Get some info of the dataset (image size, numer of offsets, investigated offset)
        % get size
        size_x = dynamic_scan.size_x;
        size_y = dynamic_scan.size_y;
        if dynamic_scan.number_of_slices ~= 1
            error('Function only valid for 2D datasets');
        end
        % get offsets
        numMeasurements = dynamic_scan.number_of_ppm_values;
        offsetsDynScan  = unique(round(dynamic_scan.ppm_values,3));
        offsetsZscan    = unique(round(initial_z_spectrum.ppm_values,3));
        numOffsetsDyn   = numel(offsetsDynScan); % get number of offsets
        ntimePoints     = numMeasurements/numOffsetsDyn;
        if mod(ntimePoints,1) ~= 0
            error('Different number of measuremnts per offsets');
        end  
        % get index of investigated offset (for glucose 1.2 ppm)
        % for Zpre and Zpost
        for m = 1:length(offsetsZscan) 
            if round(offsetsZscan(m),3) ==  conf.InvestigatedCS
                indexOfI1fullZ = m;
            elseif round(offsetsZscan(m),3) ==  -1*conf.InvestigatedCS
                indexOfI2fullZ = m;
            end
        end
        %for Dynamic scan
        for m = 1:length(offsetsDynScan) 
            if round(offsetsDynScan(m),3) ==  conf.InvestigatedCS
                indexOfI1 = m;
            elseif round(offsetsDynScan(m),3) ==  -1*conf.InvestigatedCS
                indexOfI2 = m;
            end
        end     
        if exist('indexOfI1') == 0
            warning('MyComponent:incorrectType', ['The value set in "conf.InvestigatedCS" '...
            'does not exist in the ppm list of the dynamic scan. \nAvailable options are: ']);
            warning('%.2f ',offsetsDynScan);
        end  
        
        %% Images in gray scale
        % for initial and final full Z-spectra
        initZspec = initial_z_spectrum.image_in_gray_scale;
        finZspec  = final_z_spectrum.image_in_gray_scale;
        anatomical = anatomical_scan.image_in_gray_scale;

        % for dynamic scan
        % sort dynamic scan from 4-D to 5-D;
        % from (x,y,z,ppms)=(64,96,1,1350) to (x,y,z,numOffsetsDyn, ntimePoints) = (64,96,1,9,150) 
        Z = zeros(size_x,size_y,1,numOffsetsDyn,ntimePoints);
        for ii = 1:numOffsetsDyn
            Z(:,:,1,ii,1) = dynamic_scan.image_in_gray_scale(:,:,1,ii);
            for jj = 2:ntimePoints               
                Z(:,:,1,ii,jj) = dynamic_scan.image_in_gray_scale(:,:,1,(jj-1)*numOffsetsDyn+ii);
            end
        end

        % sorts the order of the offsets (from -10, 10, -7.5, 7.5 etc to -10, -7.5, ..., 0, ..., 7.5, 10)     
       [initZspec, finZspec, offsetsZScan] = ...
        sortChemicalShifts(final_z_spectrum.ppm_values,initial_z_spectrum.ppm_values, ...
                           initZspec, finZspec);      
        
        %% generate mask (Ones and NaNs)
        
        intensityMask = true(size_x, size_y);
             
        % import whole brain ROI from folder "brainMask"
        if conf.doBrainMask == 1             
            filename = exps{e}.brainMaskFile;
            tiffImg = Tiff(filename, 'r');
            brainMask = tiffImg.read(); % Read the first image.
            brainMask = brainMask > 0;
            clear tiffImg;                         
        else
            brainMask = true(size_x, size_y);
        end
        
        % import cortex ROI from folder "cortexROI"
        if conf.doCortexROI == 1             
            filename = exps{e}.cortexROIFile;
            tiffImg = Tiff(filename, 'r');
            cortexMask = tiffImg.read(); % Read the first image.
            cortexMask = cortexMask > 0;   
            disp('Cortex ROI analysis performed');
        else
            cortexMask = true(size_x, size_y);
            disp('No cortex ROI analysis');
        end
        
        % merge the brainMask and intensityMask into one mask
        mask = brainMask &  intensityMask & cortexMask;

        %%%%%%%%%%%%%%%%%%%%%%%%%
        %   START OF ANALYSIS   %
        %%%%%%%%%%%%%%%%%%%%%%%%%
        
        %% motion correction
        % motion correction of Zpost to Zpre (the reference image is the Zpre)
        if conf.alignZSpecPost == 1
            [finZspec] = registerZSpectra(initZspec, finZspec, offsetsZScan, ...
                                 size_x, size_y, conf.MaxIterations);
        end       
        % motion correction of dynamic scan        
        if conf.motionCorr == 1  
            refImg = mean(initZspec,4);
            Z = motionCorr(conf.MaxIterations, numOffsetsDyn, ntimePoints, Z, refImg);             
        end    
 
        Z = apply2DmaskToNDdata(Z, mask);
        initZspec = apply2DmaskToNDdata(initZspec, mask);
        finZspec = apply2DmaskToNDdata(finZspec, mask);

        %% sum all pixels of the ROI together to have higher SNR curves
        if conf.meanOfAllPixels == 1
            Z = meanOfAllPixels(Z);
            initZspec = meanOfAllPixels(initZspec);
            finZspec = meanOfAllPixels(finZspec);
            mask = ones(size(mask));
        end

        %% Time smoothing
        if conf.timeSmooth == 1
            Z = timeSmoothImages(Z, conf.timeSmoothHalfWindow,conf.tSMAmethod);
        end
              
        %% Correct initial and final spectra for B0
        % for initial Z-spectrum (one time point, 47 offsets)
            [P_Zpre, dB0Pre, fitZpre,~] = ...
                generateB0map(initZspec, conf.splinesmoothing, conf.B0InterpMeth, ...
                              offsetsZScan, mask, conf.step, conf.doParallel); 

        % for final Z-spectrum (one time point, 47 offsets)            
            [P_Zpost, dB0Post, fitZpost,~] = ...
                generateB0map(finZspec, conf.splinesmoothing, conf.B0InterpMeth, ...
                              offsetsZScan, mask, conf.step, conf.doParallel);        
               
        % B0 correction of Zpre and Zpost
        P_Zpost.EVAL.w_interp=P_Zpost.SEQ.w;
        [ZPreB0corr, ~]      = B0_CORRECTION(initZspec,dB0Pre, P_Zpost);
        [ZPostB0corr, ~]     = B0_CORRECTION(finZspec, dB0Post,P_Zpost);        
        
        %% generate B0 maps
        if conf.doB0Corr == 1
            
            % for time-point Z-spectra (150 time points, 9 offsets per time point)
            % dB0 from intrepolation at each time point
            if conf.dynamicScanB0map == 1                            
                [P_Dyn, dB0Dyn, fitZDyn,~] = ...
                    generateB0map(Z, conf.splinesmoothing, conf.B0InterpMeth, ...
                                  offsetsDynScan, mask, conf.step, conf.doParallel); 
            end
        
            %keep a copy of the uncorrected for B0 dynamic Z spectra
            if conf.keepUncorrZ == 1
                ZnoB0corr = Z;
            end

            % for time-point Z-spectra (150 time points, 9 offsets per time point)
            % dB0 from linear fit Pre-post
            if conf.interpB0PrePost == 1
                [P_Lin, dB0Linear] = ...
                    generateB0mapLinearPrePpost(dB0Pre, dB0Post, ...
                                  offsetsDynScan, conf.B0InterpMeth, ntimePoints, conf.step);
            end

            %% B0 correction 
            % B0 correction of dynamic scan
            if conf.doB0Corr == 1 && strcmp(conf.B0CorrOpt, 'fullZspec') 
                P = P_Lin;
                dB0 = dB0Linear;
            elseif conf.doB0Corr == 1 && strcmp(conf.B0CorrOpt, 'dynZscan')
                P = P_Dyn;
                dB0 = dB0Dyn;
            else
                P = P_Lin;
                dB0 = dB0Linear;
            end

            P.EVAL.w_interp = P.SEQ.w;
            for t=1:ntimePoints
                if conf.meanOfAllPixels == 1
                    [Z(:,:,:,:,t), Zinterp(:,:,:,:,t)]= B0_CORRECTION((Z(:,:,:,:,t)),dB0(:,:,t),P);
                else
                    [Z(:,:,:,:,t), ~]= B0_CORRECTION((Z(:,:,:,:,t)),dB0(:,:,t),P);
                end
                t
            end
            disp('Finished B0 correction');
        
        end
        
        %% Apply mask
        for t=1:size(Z,5)
            for ppm = 1:size(Z,4)
                img = Z(:,:,1,ppm,t);
                img(~mask) = NaN;
                Z(:,:,1,ppm,t) = img;
            end
        end
        
        %% Make average of all ppms and make it 4D
        ZallPPMsAvg = mean(Z,4);
        s = size(ZallPPMsAvg);
        ZallPPMsAvg = reshape(ZallPPMsAvg,[s(1:3),s(5),1]);
        
        %% Make images of -I1nBL and (I2-I1)/I2
        meanBLimage = mean(Z(:,:,:,indexOfI1,conf.BaselineImgs),5);
        dim = size(Z);
        dim(4) = [];
        imageNegI1nBL = zeros(dim);
        for t=1:size(Z,5)
            for z=1:size(Z,3)
                imageNegI1nBL(:,:,z,t) = Z(:,:,z,indexOfI1,t)./meanBLimage;
            end
        end
        
        imageRatio = zeros(dim);
        for t=1:size(Z,5)
            for z=1:size(Z,3)
                imageRatio(:,:,z,t) = Z(:,:,z,indexOfI1,t)./Z(:,:,z,indexOfI2,t);
            end
        end
              
        %% make tables of data
        % make timescale in min (for a period of 36 sec: 36/60 = 0.6)
        for timeIdx = 1: size(Z,5)
            timescale(timeIdx) = timeIdx * 0.6;
        end
         
        if conf.meanOfAllPixels == 1
            % STAsym curves table
            ZPreTable  = makefullZTable(squeeze(initZspec), squeeze(ZPreB0corr), offsetsZScan);
            ZPostTable = makefullZTable(squeeze(finZspec), squeeze(ZPostB0corr), offsetsZScan);
            for timeIndx = 1:size(Z,5)
                ZDynTable{timeIndx}  = makeZTable(squeeze(Z(1,1,1,:,timeIndx)), offsetsDynScan);
                if conf.keepUncorrZ == 1
                    ZDynNoCorrTable{timeIndx}  = makeZTable(squeeze(ZnoB0corr(1,1,1,:,timeIndx)), offsetsDynScan); 
                end
                ZrefToFirsrt = Z(1,1,1,:,timeIndx)-mean(Z(1,1,1,:,6:16),5);
                ZDynReferenceTable{timeIndx}  = makeZTable(squeeze(ZrefToFirsrt), offsetsDynScan);

                ZrefToFirsrt2 = Zinterp(1,1,1,:,timeIndx)-mean(Zinterp(1,1,1,:,6:16),5);
                ZDynInterpReferenceTable{timeIndx}  = makeZTable(squeeze(ZrefToFirsrt2), P_Dyn.EVAL.w_fit);
            end 

            % STAsym curve table
            STAsymDyn = STAsymTable(squeeze(Z(1,1,1,indexOfI1,:)), ...
                        squeeze(Z(1,1,1,indexOfI2,:)), timescale, conf.BaselineImgs);          
        else
            warning('pixel-wise analysis is not implemented');
        end    
        
        if DEBUG_SAVE == 1
            save(DEBUG_FILE);
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% PREPARE OUTPUT ENVIRONMENT %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    %% Make output dirs
    [outSubDir, plotDir, textDir, DynPlotDir, DynPlotDir2] = ...
              makeOutdirs3(exps{e}.workdir, conf, exps{e}.shortName);

    disp(outSubDir);
    
    %% Copy the conf file in plotDir dir to keep track of analysis conditions
    copyfile(conf.configPath, outSubDir);                                          
    
    %% print stuff in directory Results/images   
    %print Anatomical Scan in Results/images
    printImageFloat32(plotDir, anatomical, 'AnatomicalGS.tif',indexOfI1)

    if conf.meanOfAllPixels == 0
        %print MRI Dynamic Scan Image in Results/images
        if conf.printOnlyInvestigatedCS == 1
            CSname = strrep(num2str(offsetsDynScan(indexOfI1)),'.','p');
            printImageFloat32(plotDir, Z, ['MRIDynamicScan_',CSname,'ppm_',conf.outName,'.tif'],indexOfI1)
        else
            for idxCS = 1:numOffsetsDyn
                CSname = strrep(num2str(offsetsDynScan(idxCS)),'.','p');
                printImageFloat32(plotDir, Z, ['MRIDynamicScan_',CSname,'ppm_',conf.outName,'.tif'],idxCS)
            end          
        end

        % print MRI Zpre and Zpost Images in Results/images
        % print Zpre at investigated offset
        image = initZspec(:,:,1,indexOfI1fullZ);
        CSname = strrep(num2str(offsetsZscan(indexOfI1fullZ)),'.','p');
        printImageFloat32(plotDir,image, ['MRIZpre_',CSname,'ppm_',conf.outName,'.tif'])
        % print Zpre all offsets
        image = initZspec;
        printImageFloat32(plotDir,image, ['MRIfullZpre_',conf.outName,'.tif'])
        % print Zpost at investigated offset
        image = finZspec(:,:,1,indexOfI1fullZ);
        CSname = strrep(num2str(offsetsZscan(indexOfI1fullZ)),'.','p');
        printImageFloat32(plotDir,image, ['MRIZpost_',CSname,'ppm_',conf.outName,'.tif'])
        % print Zpost all offsets
        image = finZspec;
        printImageFloat32(plotDir,image, ['MRIfullZpost_',conf.outName,'.tif'])

        %print dB0 of Zpre, Zpost and DynScan and LinearFit
        if conf.doB0Corr == 1
            printImageFloat32(plotDir,dB0Pre, ['dB0Zpre_',conf.outName])
            printImageFloat32(plotDir,dB0Post, ['dB0Zpost_',conf.outName])
            printImageFloat32(plotDir,dB0Post-dB0Pre, ['dB0Zpost-dB0Zpre_',conf.outName])
            printImageFloat32(plotDir,dB0Dyn, ['dB0Dyn_',conf.outName])
        end

        %print Dynamic Glucose Enchancement Image in Results/images
        printImageFloat32(plotDir, ZallPPMsAvg, ['DGEallPPMavg_',conf.outName]);
        if conf.printDGEimages == 1
            printImageFloat32(plotDir, imageNegI1nBL, ['I1nBL_raw_',conf.outName]);
            printImageFloat32(plotDir, imageRatio, ['I2I1ratio_raw_',conf.outName]);
            printImageIntWeightRGB(plotDir, imageNegI1nBL, ZallPPMsAvg, ['I1nBL_IW_',conf.outName], ...
                                   conf.RGBimgs, conf.RGBimgs.I1nBL.cmapMin, conf.RGBimgs.I1nBL.cmapMax, ppm);

            printImageIntWeightRGB(plotDir, imageRatio, ZallPPMsAvg, ['I2I1ratio_IW_',conf.outName], ...
                                   conf.RGBimgs, conf.RGBimgs.I2I1.cmapMin, conf.RGBimgs.I2I1.cmapMax, ppm);
        end      
        warning('plotting Zspectra with averaged values from pixel-wise analysis is not implemented');

    elseif conf.meanOfAllPixels == 1  
        % print full Zspectra in Results/images
        printZSpectra(plotDir, ZPreTable, ZPostTable, exps{e}.shortName)
       
        % Print STAsym curves in Results/images
        CSname = strrep(num2str(conf.InvestigatedCS),'.','p'); % make string with investigated offset name        
        partName = [exps{e}.shortName, '_STAsym_',CSname,'ppm'];
        printSTAsymCurves(plotDir, STAsymDyn, partName, conf.InvestigatedCS) 

        %print Dynamic Zspectra in Results/images/DynZspectra
        if conf.printDynZspectra == 1
            for timeIndx = 1:size(Z,5)
                Ymax(timeIndx) = max( ZDynTable{timeIndx}.DynZspec);
                Ymin(timeIndx) = min( ZDynTable{timeIndx}.DynZspec);
                Ymax2(timeIndx) = max( ZDynReferenceTable{timeIndx}.DynZspec);
                Ymin2(timeIndx) = min( ZDynReferenceTable{timeIndx}.DynZspec);
            end
            Ymax = max(Ymax);
            Ymin = min(Ymin);
            Ymax2 = max(Ymax2);
            Ymin2 = min(Ymin2);
            for timeIndx = 1:size(Z,5) 
                if timeIndx < 10
                    Indx =['00',num2str(timeIndx)];
                elseif timeIndx < 100
                    Indx =['0',num2str(timeIndx)];
                else
                    Indx =num2str(timeIndx);
                end
                partName = [exps{e}.shortName, '_ZSpectrum',Indx];              
                if conf.keepUncorrZ == 0
                     printSingleZSpectrum(DynPlotDir, ZDynTable{timeIndx}, partName,Ymax,Ymin)                    
                else
                    printFitZSpectrum   (DynPlotDir, ZDynTable{timeIndx}, squeeze(Zinterp(:,:,:,:,timeIndx)), P_Dyn, partName,Ymax,Ymin)
                    printSingleZSpectrum(DynPlotDir2, ZDynInterpReferenceTable{timeIndx}, partName,Ymax2,Ymin2)
                end
            end
        end
                
        %% Print data in Results/text
        % Print the STAsym results table
        resultsPath = strcat(textDir, '/STAsym.xlsx');
        warning('off');
        writetable(STAsymDyn, resultsPath, 'sheet', 'STAsymDyn');
        deleteXlsSheets(resultsPath,{'Sheet1','Sheet2','Sheet3'}); % Fix for Matlab leaving default Xls sheets
        warning('on');
                  
        % Print the ZPre results table
        resultsPath = strcat(textDir, '/ZPreZPost.xlsx');
        warning('off');
        writetable(ZPreTable, resultsPath, 'sheet', 'ZPre');
        writetable(ZPostTable, resultsPath, 'sheet', 'ZPost');
        deleteXlsSheets(resultsPath,{'Sheet1','Sheet2','Sheet3'}); % Fix for Matlab leaving default Xls sheets
        warning('on');
    end
    toc
    end
    if conf.disableWarnings == 1
        warning('on')
    end
end    