function [] = printImageFloat32(folder, image, name, ppm)

imSize = size(image);
imDims = ndims(image);

% Define names and open files 
tiffImg = strcat(folder, '/', name, '.tif');
tiffImg = Tiff(tiffImg, 'w');

% Add frames to the image
if imDims == 2
    addFloat32frame(tiffImg, image(:,:))
elseif imDims == 3
    for i=1:imSize(3)
        addFloat32frame(tiffImg, image(:,:,i))
    end   
elseif imDims == 4
    for i=1:imSize(4)
        addFloat32frame(tiffImg,image(:,:,:,i))
    end
elseif imDims == 5
    for i=1:imSize(5)
        addFloat32frame(tiffImg,image(:,:,:,ppm,i))
    end 
end

% Close images 
tiffImg.close();

end

