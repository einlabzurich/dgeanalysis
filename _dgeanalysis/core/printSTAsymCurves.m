function [] = printSTAsymCurves(plotDir, resultsTable, partName, CS)

x = resultsTable.time;
x_label = 'time / min';

%make figure
figName = [plotDir, '/', partName, '.jpg'];
fig = figure('pos',[100 100 1024 1024], 'visible', 'off');

Xmax = max(x);
Xmin = min(x);

subplot(2,2,1);
    plot(x, resultsTable.I1nBL, 'k');
    hold on
    plot(x, resultsTable.I2nBL, 'r');
    
    Ymax = max(max(resultsTable.I1nBL,resultsTable.I2nBL))*1.005;
    Ymin = min(min(resultsTable.I1nBL,resultsTable.I2nBL))*0.99;

    xlim([Xmin, Xmax]);
    ylim([Ymin, Ymax]);

    xlabel(x_label);
    ylabel("Water signal / a.u.");
     title(['Dynamic curve at ',num2str(CS),' (black) and -', num2str(CS),' ppm (red)']);

subplot(2,2,2);
    plot(x, resultsTable.I1negnBL, 'k');

    Ymax = max(resultsTable.I1negnBL)*0.99;  
    Ymin = min(resultsTable.I1negnBL)*1.005;

    xlim([Xmin, Xmax]);
    ylim([Ymin, Ymax]);

    xlabel(x_label);
    ylabel("Water signal / a.u.");
     title(['-I / I0 at ', num2str(CS), ' ppm']);

subplot(2,2,3);
    plot(x, resultsTable.diffI2I1, 'k');

    Ymax = max(resultsTable.diffI2I1)*1.05;
    Ymin = min(resultsTable.diffI2I1)*0.9;

    xlim([Xmin, Xmax]);
    ylim([Ymin, Ymax]);

    xlabel(x_label);
    ylabel("Water signal / a.u.");
    title('I2-I1');

subplot(2,2,4);
    plot(x, resultsTable.diffI2I1NormI2, 'k');

    Ymax = max(resultsTable.diffI2I1NormI2)*1.05;
    Ymin = min(resultsTable.diffI2I1NormI2)*0.9;

    xlim([Xmin, Xmax]);
    ylim([Ymin, Ymax]);

    xlabel(x_label);
    ylabel("Water signal / a.u.");
    title('(I2-I1)/I2');
    
% Save the figure
saveas(fig, figName);


end

