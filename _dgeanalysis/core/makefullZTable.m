function [Tables] = makefullZTable(noB0corr, B0corr, ppms)

colNames = {'offsets', 'noB0corr', 'B0corr'};
Matrix   = [ppms', noB0corr, B0corr];

% Make the table
Tables = array2table(Matrix, 'VariableNames', colNames);

end

