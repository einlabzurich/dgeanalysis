function [data] = apply2DmaskToNDdata(data, mask)

dims = size(data);
fullmask = repmat(mask, [1 1 dims(3:end)]);
data(~fullmask) = nan;

end

