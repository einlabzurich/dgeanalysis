function[parameters] = read_reco(folder_number,directory)

% goes to the directory of the scan folder (the name of the folder is a number)
folder_number = num2str(folder_number); %folder_number is the index that is being looped for all scans in the main program
directory_scan = strcat(directory,'\',folder_number); %directory of the scan
directory_reco = strcat(directory_scan,'\pdata\1');

filename = 'reco';
file_reco = textread(fullfile(directory_reco, filename),'%s','delimiter','=','whitespace','');

size_reco=size(file_reco);

parameters.directory_reco_and_2dseq = directory_reco;
parameters.slope_values = [];

for i=1:size_reco(1)
    if strcmp(file_reco(i),'##$RECO_fov')
        parameters.FOV = str2num(char(file_reco(i+2))); % in cm
        parameters.FOV = parameters.FOV * 10;           % in mm
    end    
    if strcmp(file_reco(i),'##$RECO_size')
        size_matrix = str2num(char(file_reco(i+2)));
        parameters.size_x = size_matrix(2);
        parameters.size_y = size_matrix(1); 
    end    
    if strcmp(file_reco(i),'##$RECO_wordtype')
        parameters.reco_wordtype = (char(file_reco(i+1)));
    end
    if strcmp(file_reco(i),'##$RECO_map_slope')
        number_of_slope_values = str2num(char(file_reco(i+1)));
        
        %extracts all #slope_index slopes. 
        i=i+1;
        slopes_in_one_line      = (str2num(char(file_reco(i+1))));                    
        parameters.slope_values = horzcat(parameters.slope_values,slopes_in_one_line);
        size_of_slope_values    = size(parameters.slope_values);
        i=i+1;
        while size_of_slope_values(2) < number_of_slope_values
            slopes_in_one_line      = (str2num(char(file_reco(i+1))));  
            parameters.slope_values = horzcat(parameters.slope_values,slopes_in_one_line);
            size_of_slope_values    = size(parameters.slope_values);
            i=i+1;
        end        
    end
end

if strcmp(parameters.reco_wordtype,'_32BIT_SGN_INT')
    parameters.reco_bit='uint32';
elseif strcmp(parameters.reco_wordtype,'_16BIT_SGN_INT')
    parameters.reco_bit='uint16';
end

parameters.slope = parameters.slope_values(1);

clear number_of_slope_values size_of_slope_values slopes_in_one_line
clear directory_reco directory_scan
end