function [] = printSingleZSpectrum(plotDir, ZTable, shortName,Ymax,Ymin)

x = ZTable.offsets;
x_label = 'Offset / ppm';

% Make the figure    
figName = strcat(plotDir, '/', shortName, '.jpg');
fig = figure('pos',[100 100 1024 1024], 'visible', 'off');

Xmax = max(x);
Xmin = min(x);

plot(x, ZTable.DynZspec, '-k');
hold on

Ymax = Ymax *1.1;
Ymin = Ymin *0.9;

xlim([Xmin, Xmax]);
ylim(sort([Ymin, Ymax])); % Sort is needed to avoid crash when numerical instabilities give negative numbers

xlabel(x_label);
ylabel(' Water signal / a.u.');

% Save the figure
saveas(fig, figName);
    
end


