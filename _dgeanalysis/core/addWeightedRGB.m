function [RGB, Cmap, MapToColorScale] = addWeightedRGB(intWeighted, image, ref, outName, varargin)
% WARNING: This function produces an RGB image in which the information in
% the original MIGHT BE (PARTIALLY) LOST. The Colormap (Cmap) and the map of 
% ratios-to-color (MapToColorScale) have the SOLE PURPOSE OF PLOTTING A
% COLORBAR. The output RGB image can be used for VISUAL INSPECTION ONLY,
% and NO FURTHER DATA ANALYSIS SHOULD BE CONDUCTED ON IT.

persistent p;
if isempty(p)
    p = inputParser;
    p.FunctionName = 'makeWeightedRGB';
    p.addRequired('intWeighted');
    p.addRequired('image');
    p.addRequired('ref');
    p.addRequired('outName');
    p.addParameter('cmapString', 'jet');
    p.addParameter('brightScaleFactor', 1);
    p.addParameter('cmDepth', 256);
    p.addParameter('cmapMin', -1); % For some reason it complains if I put min(image(:)) here.
    p.addParameter('cmapMax', -1);
    p.addParameter('metadata', -1);
    p.addParameter('highlightSaturation', 0);
end

parse(p, intWeighted, image, ref, outName, varargin{:});

cmapString = p.Results.cmapString;
brightScaleFactor = p.Results.brightScaleFactor;
cmDepth = p.Results.cmDepth;
cmapMin = p.Results.cmapMin;
cmapMax = p.Results.cmapMax;
metadata = p.Results.metadata;
highlightSaturation = p.Results.highlightSaturation;

% if isempty(image(~isnan(image(:)))) %% The image is all NaNs, because of exclusion on motion correction
%     image = zeros(size(image));
% end
% 
% if cmapMin == -1
%     cmapMin = min(image(~isinf(image(:)) & ~isnan(image(:))));
% end
% if cmapMax == -1
%     cmapMax = max(image(~isinf(image(:)) & ~isnan(image(:))));
% end

[RGB, Cmap, MapToColorScale] = IntWeightRGB8(image, ref, ...
                                         'cmapString', cmapString, ...
                                         'brightScaleFactor', brightScaleFactor, ...
                                         'cmDepth', cmDepth, ...
                                         'cmapMin', cmapMin, 'cmapMax', cmapMax, ...
                                         'nanColor', 'black', ...
                                         'highlightSaturation', highlightSaturation);

% Define Tiff properties
tagstruct.ImageLength = size(RGB(:,:,1),1);
tagstruct.ImageWidth = size(RGB(:,:,1),2);
tagstruct.SampleFormat = Tiff.SampleFormat.UInt; 
tagstruct.Photometric = Tiff.Photometric.RGB;
tagstruct.BitsPerSample = 8;
tagstruct.SamplesPerPixel = 3;
tagstruct.Compression = Tiff.Compression.None;  
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky; 
tagstruct.Software = 'MATLAB'; 

% Get Image Description (ScanImage metadata)
if isstruct(metadata)
    tagstruct.ImageDescription = metadata.ImageDescription;
end

% Add Tiff image
setTag(intWeighted, tagstruct);
write(intWeighted, RGB);
writeDirectory(intWeighted);


end

