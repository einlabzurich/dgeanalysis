function [P,dB0] = generateB0mapLinearPrePpost(dB0Pre, dB0Post, offsets, B0InterpMeth, ntimePoints, step)

P.SEQ.w                = offsets;
P.EVAL.w_fit           = min(P.SEQ.w):step:max(P.SEQ.w);
P.EVAL.w_interp        = P.SEQ.w;
P.EVAL.B0_int_meth     = B0InterpMeth; %usually 'linear'

for ii=1:size(dB0Pre,1)
  for jj=1:size(dB0Pre,2)
    dB0(ii,jj,:) = interp1([1 ntimePoints],[squeeze(dB0Pre(ii,jj)) squeeze(dB0Post(ii,jj)) ],1:ntimePoints);
  end
end
end