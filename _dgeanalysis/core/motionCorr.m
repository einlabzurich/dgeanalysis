function [image] = motionCorr(MaximumIterations,numOffsets,ntimePoints,image,refImg)
% motion correction is done in each chemical shift image using 
% the first time-point images (9 first images)
[optimizer, metric] = imregconfig('monomodal'); %intensity-based registration
optimizer.MaximumIterations = MaximumIterations;
tforms = cell(numOffsets,ntimePoints);
x = size(image,1);
y = size(image,2);

sumSrcImage = mean(image,4);

for jj = 2:ntimePoints
    tforms{jj} = imregtform(sumSrcImage(:,:,1,1,jj), refImg, 'rigid', optimizer, metric);
    for ii = 1:numOffsets
         srcImg = image(:,:,1,ii,jj); 
         image(:,:,1,ii,jj) = imwarp(srcImg,tforms{jj},'interp', 'cubic', 'OutputView',imref2d([x,y]));
    end
end  

end