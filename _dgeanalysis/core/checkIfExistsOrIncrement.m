function [name] = checkIfExistsOrIncrement(name, type, overrideSwitch, suffix)

% If optional arguments are not specified, use default values
if nargin < 4
    suffix = '';
end
fullname = strcat(name, suffix);

if exist(fullname,type)
   if overrideSwitch == 1
       % Name already exists and is used as output name
   else
       % Add a progressive number at the end of the name
       count = 1;
       originalName = name;
       while exist(fullname,type)
          name = strcat(originalName, '_', num2str(count, '%03.f'));
          fullname = strcat(name, suffix);
          count = count + 1;
       end 
   end
end

end

