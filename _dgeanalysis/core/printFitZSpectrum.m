function [] = printFitZSpectrum(plotDir, B0corr, B0corr_interp, P, shortName,Ymax,Ymin)

x = B0corr.offsets;
xFit = P.EVAL.w_fit; 
x_label = 'Offset / ppm';

% Make the figure    
figName = strcat(plotDir, '/', shortName, '.jpg');
fig = figure('pos',[100 100 1024 1024], 'visible', 'off');

Xmax = max(x);
Xmin = min(x);

plot(xFit,B0corr_interp, '-k');

Ymax = Ymax *1.1;
Ymin = Ymin *0.9;

xlim([Xmin, Xmax]);
ylim(sort([Ymin, Ymax])); % Sort is needed to avoid crash when numerical instabilities give negative numbers

xlabel(x_label);
ylabel(' Water signal / a.u.');

% Save the figure
saveas(fig, figName);
    
end


