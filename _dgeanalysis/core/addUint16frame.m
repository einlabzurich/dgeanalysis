function addUint16frame(TiffImage, frame, varargin)
% WARNING: This function produces an RGB image in which the information in
% the original MIGHT BE (PARTIALLY) LOST. The Colormap (C) and the map of 
% ratios-to-color (MapToColorScale) have the SOLE PURPOSE OF PLOTTING A
% COLORBAR. The output RGB image can be used for VISUAL INSPECTION ONLY,
% and NO FURTHER DATA ANALYSIS SHOULD BE CONDUCTED ON IT.

persistent p;
if isempty(p)
    p = inputParser;
    p.FunctionName = 'addUint16frame';
    p.addRequired('TiffImage');
    p.addRequired('frame');
    p.addParameter('metadata', -1);
end

parse(p, TiffImage, frame, varargin{:});
metadata = p.Results.metadata;

% Define Tiff properties
tagstruct.ImageLength = size(frame, 1);
tagstruct.ImageWidth = size(frame, 2);
tagstruct.SampleFormat = Tiff.SampleFormat.UInt; 
tagstruct.Photometric = Tiff.Photometric.MinIsBlack; 
tagstruct.BitsPerSample = 16;
tagstruct.SamplesPerPixel = 1;
tagstruct.Compression = Tiff.Compression.None; 
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
tagstruct.Software = 'MATLAB'; 

% Get Image Description (ScanImage metadata)
if isstruct(metadata)
    tagstruct.ImageDescription = metadata.ImageDescription;
end

% Add Tiff image
setTag(TiffImage, tagstruct);
write(TiffImage, uint16(frame));
writeDirectory(TiffImage);

end

