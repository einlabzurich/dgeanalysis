function [RGB, C, MapToColorScale] = IntWeightRGB8(image, refData, varargin)
% WARNING: This function produces an RGB image in which the information in
% the original MIGHT BE (PARTIALLY) LOST. The Colormap (C) and the map of 
% ratios-to-color (MapToColorScale) have the SOLE PURPOSE OF PLOTTING A
% COLORBAR. The output RGB image can be used for VISUAL INSPECTION ONLY,
% and NO FURTHER DATA ANALYSIS SHOULD BE CONDUCTED ON IT.

persistent p;
if isempty(p)
    p = inputParser;
    p.FunctionName = 'IntWeightRGB';
    p.addRequired('image');
    p.addRequired('refData');
    p.addParameter('cmapString', 'jet');
    p.addParameter('brightScaleFactor', 1);
    p.addParameter('cmDepth', 256);
    p.addParameter('cmapMin', -1);
    p.addParameter('cmapMax', -1);
    p.addParameter('nanColor', 'black');
    p.addParameter('highlightSaturation', 0);
end

parse(p, image, refData, varargin{:});

cmapString = p.Results.cmapString;
brightScaleFactor = p.Results.brightScaleFactor;
cmDepth = p.Results.cmDepth;
cmapMin = p.Results.cmapMin;
cmapMax = p.Results.cmapMax;
nanColor = p.Results.nanColor;
highlightSaturation = p.Results.highlightSaturation;

isAllNaNs = 0;
if isempty(image(~isnan(image(:)))) %% The image is all NaNs, because of exclusion on motion correction
    isAllNaNs = 1;    
    image = zeros(size(image));
end

if cmapMin == -1
    cmapMin = min(image(~isinf(image(:)) & ~isnan(image(:))));
end
if cmapMax == -1
    cmapMax = max(image(~isinf(image(:)) & ~isnan(image(:))));
end

if cmapMin < 0 %FOR CASES CONTAINING NEGATIVE NUMBERS (WHERE DO THEY COME FROM?)
    cmapMin = 0;
end


% Bring all values above cmapMax to cmapMax (and same with cmapMin). This is needed
% to avoid crash, but anyways there is no loss of info, since the modified
% data are anyways just "beyond the limits". Their actual value is
% irrelevant, since the purpose of the produced image is PURELY VISUAL. 
% disp(size(image))
% disp(cmapMax)
maxMask = image > cmapMax;
minMask = image < cmapMin;
image(maxMask) = cmapMax;
image(minMask) = cmapMin;

% Normalize refernce data
refData = refData./max(refData(:));

% Define colormap
black = [0, 0, 0];
white = [1, 1, 1];

if isAllNaNs == 0
    if highlightSaturation == 1
        C = advancedColormap(cmapString, cmDepth-2);
        C = [black; C; white];
    else
        C = advancedColormap(cmapString, cmDepth-1);
        C = [black; C];
    end

    MapToColorScale = linspace(cmapMin,cmapMax,cmDepth);
    I = interp1(MapToColorScale, 1:cmDepth, image, 'linear');
    Gs = round(I);

    % Correct for NaNs and Infs (WHERE DO THEY COME FROM?)
    infmask = isinf(Gs);
    nanmask = isnan(Gs);
    if strcmp(nanColor, 'black')
        Gs(infmask) = 1;
        Gs(nanmask) = 1;
        refData(infmask) = 0;
        refData(nanmask) = 0;
    elseif strcmp(nanColor, 'white')
        Gs(infmask) = cmDepth;
        Gs(nanmask) = cmDepth;
    else
        warning("Wrong value for nanColor, options are 'white' and 'black'. Defaulting to 'black'");
        Gs(infmask) = 1;
        Gs(nanmask) = 1;
        refData(infmask) = 0;
        refData(nanmask) = 0;
    end
    RGB = reshape(C(Gs,:),[size(Gs) 3]); 
else
    C = black;
    MapToColorScale = 0;
    RGB = zeros([size(image) 3]); 
end

% Convert RGB to YCbCr and use ref image as brightness
YCbCr = single(rgb2ycbcr(RGB));
YCbCr(:,:,1) = brightScaleFactor * refData;

% Reconvert to RGB (8-bit depth)
RGB = ycbcr2rgb(YCbCr);
RGB = uint8(RGB.*256);

end

