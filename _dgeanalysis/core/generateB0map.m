function [P,dB0, yRaw, yFit] = generateB0map(Z, splinesmoothing, B0InterpMeth, ppmVals, mask, step, doParallel)
 
P.EVAL.splinesmoothing = splinesmoothing; % smoothing Parameter for smoothing spline
P.EVAL.B0_int_meth     = B0InterpMeth;
P.SEQ.w                = ppmVals';
P.EVAL.w_fit           = min(P.SEQ.w):step:max(P.SEQ.w);
P.EVAL.w_interp        = P.EVAL.w_fit;

yFit = 0;
% ndims(Zspec) == 4 is true in the case of full Z-spectra (with 47 offsets)
if ndims(Z) == 4
    [dB0, yRaw, ~] = MINFIND_SPLINE_3D(Z, mask, P, doParallel);

% ndims(Zspec) == 5 is true in the case of dynamic Z-spectra (with only 9 offsets)
elseif ndims(Z) == 5
    for i = 1:size(Z,5) %the 5th dimension has the time points, 150 for a full experiment
    [dB0(:,:,i), yRaw(:,:,:,:,i), ~] = MINFIND_SPLINE_3D(Z(:,:,:,:,i), mask, P, doParallel);
    end
end
end
