function[parameters] = read_2dseq(folder_number,directory,par_reco,par_acqp)

folder_number = num2str(folder_number); %folder_number is the index that is being looped for all scans in the main program
directory_scan = strcat(directory,'\',folder_number); %directory of the scan
directory_2dseq = strcat(directory_scan,'\pdata\1\');

filename = '2dseq';

fid=fopen(fullfile(directory_2dseq, filename));

%store the images in 'parameters.image_2dseq'
for k=1:par_acqp.number_of_ppm_values %Loop over each ppm image of a scan
   for i=1:par_reco.size_x %Loop over x dimension of image
       for j=1:par_reco.size_y %Loop over y dimension of image
           parameters.image_2dseq(i,j,1,k)=fread(fid,1,par_reco.reco_bit);
       end
   end
end
fclose(fid);

%scales all image values for the correction factor
parameters.image_2dseq = parameters.image_2dseq/par_reco.slope;

%gray scale 2dseq image
parameters.image_in_gray_scale = mat2gray(parameters.image_2dseq(:,:,:,:));

clear fid directory_2dseq
end