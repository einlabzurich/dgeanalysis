function [data] = timeSmoothImages(data, smoothHalfWindow, method)

smoothWindow = 2*smoothHalfWindow + 1;
data = smoothdata(data, 5, method, smoothWindow);

end

