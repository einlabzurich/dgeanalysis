function [imageB0corr,imageB0corrInterp] = B0_CORRECTION(image,dB0,P)

sizes=size(image);
imageB0corr = zeros(sizes(1),sizes(2),sizes(3),numel(P.SEQ.w));  
imageB0corrInterp = zeros(sizes(1),sizes(2),sizes(3),numel(P.EVAL.w_fit));  

for kk=1:sizes(3)          %z
  for ii=1:sizes(1)        %x
    for jj=1:sizes(2)      %y
      if ~isnan(image(ii,jj,kk,1,1))
        tmpzspec=squeeze(image(ii,jj,kk,:));
        if ~isnan(squeeze(dB0(ii,jj,kk)))
          [imageB0corr(ii,jj,kk,:),imageB0corrInterp(ii,jj,kk,:)]=B0_CORRECTION_1D(tmpzspec,dB0(ii,jj,kk),P);
        end            
      end            
    end
  end
end
end