function [ZspecPost] = registerZSpectra(ZspecPre, ZspecPost, ppmVals, x,y,MaximumIterations)
   
    [optimizer, metric] = imregconfig('monomodal'); %intensity-based registration
    optimizer.MaximumIterations = MaximumIterations;
    for ii = 1:numel(ppmVals)
        %align ZspecPost to ZspecPre and get the geometric transformation map "tform"
        tform = imregtform(ZspecPost(:,:,ii), ZspecPre(:,:,ii), 'rigid', optimizer, metric);
        ZspecPost(:,:,ii) = imwarp(ZspecPost(:,:,ii),tform,'interp', 'cubic', 'OutputView',imref2d([x,y]));
    end
    


end

