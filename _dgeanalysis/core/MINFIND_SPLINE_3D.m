function [dB0, yFitOnlyInOffsets, yFit]= MINFIND_SPLINE_3D(data,mask,P, doParallel)
tic
fittoolboxflag = 1;  %% is the fitting toolbox installed or not

[x,y,z,NumOffset] = size(data);

xFit(:,1)         = P.EVAL.w_fit; % the ppm values with the step: conf.step 
xRaw              = P.SEQ.w;      % the actual ppm values only
dB0               = zeros(x, y, z);
yFitOnlyInOffsets = zeros(x,y,z,length(xRaw));
yFit              = zeros(x,y,z,length(xFit));
tempZspec         = zeros(NumOffset,1);
    
%if "Composite" exists then do parallel programming otherwise do not parallel
if (exist('Composite')>0) && doParallel == 1
    %% Disassemble matrices on labs 
    Stack_c = Composite();  % One element per lab in the pool
    yS_c    = Composite();
    yfit_c  = Composite();    
    dB0     = Composite();
    mask_c  = Composite();
    
    npools = size(Stack_c,2); % how many matlab pools
    np     = y/npools;    
    %teilintervalle
    IV = @(i) 1+fix(np*(i-1)):fix(np*(i)); %InterVals
    
    for ii = 1:length(Stack_c)
        Stack_c{ii} = data(:,IV(ii),:,:);       
        yS_c{ii}    = zeros(x,numel(IV(ii)),z,NumOffset);
        yfit_c{ii}  = zeros(x,numel(IV(ii)),z,numel(xFit) );        
        dB0{ii}     = zeros(x,numel(IV(ii)),z);        
    end
    
    %% iteration in matrix parts (parallel programming)
    spmd       
    fo_ = fitoptions('method','SmoothingSpline','SmoothingParam',P.EVAL.splinesmoothing);
    ft_ = fittype('SmoothingSpline');
    for k=1: z             
      for i=1: x
        for j=1:numel(IV(labindex))
%           if mask_c(i,j,k) > 0
          if ~isnan(Stack_c(i,j,k,1,1))
            tempZspec(:,1)=squeeze(Stack_c(i,j,k,:));
            %fit for in vivo
            cf_             = fit(xRaw,tempZspec,ft_,fo_);
            yS_c(i,j,k,:)   = cf_(xRaw);
            yfit_c(i,j,k,:) = cf_(xFit);
            
            [minval,minind] = min(yfit_c(i,j,k,:));
            dB0_c(i,j,k)    = xFit(minind,1);
          else
            dB0_c(i,j,k) = NaN;
          end
        end
      end                
    end  % end for slice loop 
    end
        
    %% reallocation partitions to local
    data              = cat(2, Stack_c{:});
    yFitOnlyInOffsets = cat(2, yS_c{:});
    yFit              = cat(2, yfit_c{:});
    dB0               = cat(2, dB0_c{:});    
else  %not parallel          
    fo_ = fitoptions('method','SmoothingSpline','SmoothingParam',P.EVAL.splinesmoothing);
    ft_ = fittype('SmoothingSpline');

    % Preallocate for speed        
    for k=1:z        
      for i=1:x 
        for j=1:y 
%           if mask(i,j,k) == 1 || (x == 1 && y ==1)
          if ~isnan(data(i,j,k,1,1)) || (x == 1 && y ==1)
            tempZspec(:,1)=squeeze(data(i,j,k,:));                    
            if fittoolboxflag
                cf_                       = fit(xRaw,tempZspec,ft_,fo_);                       
                yFitOnlyInOffsets(i,j,k,:)= cf_(xRaw);
                yFit(i,j,k,:)             = cf_(xFit);
            else
                yFitOnlyInOffsets(i,j,k,:) = interp1(xRaw,bfilt(tempZspec)',xRaw,'spline');
                yFit(i,j,k,:)              = interp1(xRaw,bfilt(tempZspec)',xFit,'spline');
            end
            [minval,minind] = min(yFit(i,j,k,:));
            dB0(i,j,k) = xFit(minind,1);                    
          else
            dB0(i,j,k)=NaN;
          end                
        end            
      end
    end
    
end
toc
end

