function [meanOfImage] = meanOfAllPixels(image)

meanOfImage = nanmean(nanmean(image,1),2);

end

