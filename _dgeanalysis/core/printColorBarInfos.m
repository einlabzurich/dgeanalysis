function [] = printColorBarInfos(Cmap, MapToColorScale, outName, varargin)

persistent p;
if isempty(p)
    p = inputParser;
    p.FunctionName = 'printColorBarInfos';
    p.addRequired('Cmap');
    p.addRequired('MapToColorScale');
    p.addRequired('outName');
    p.addParameter('printCB', 1);
    p.addParameter('printCMInfo', 1);
end

parse(p, Cmap, MapToColorScale, outName, varargin{:});
printCB = p.Results.printCB;
printCMInfo = p.Results.printCMInfo;

if printCB == 1
    cbarFig = figure('pos',[100 100 100 300], 'visible', 'off');
    ax = axes;
    ax.Visible = 'off';
    colormap(gca, Cmap);
    c = colorbar(ax);
    pos = c.Position;
    pos(1) = 0.4; %left
    pos(3) = 0.15; % width
    c.LineWidth = 1.5;
    c.TickDirection = 'out';
    c.TickLength = 0.02;
    c.FontSize = 10;
    c.FontWeight = 'bold';
    c.AxisLocation = 'in';
    c.Position = pos;
    Imin = MapToColorScale(1);
    Imax = MapToColorScale(end);
    caxis([Imin, Imax]);
    figName = strcat(outName, '_CB.png');
    saveas(cbarFig, figName);
end

if printCMInfo == 1
   csvName = strcat(outName, '_CMinfo.csv');
   CMinfo = [Cmap, MapToColorScale'];
   dlmwrite(csvName, CMinfo);  
end

end

