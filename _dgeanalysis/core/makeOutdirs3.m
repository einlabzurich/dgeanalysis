function [outSubDir, plotDir, textDir, DynPlotDir, DynPlotDir2] = makeOutdirs3(workdir, conf, shortName)

% Redefine variables for readability
outDir = strcat(workdir, '/Results');
outName = strcat(conf.outName);
% outName = strcat(conf.outName,'_',shortName);

% Make output main folder
if ~exist(outDir,'dir')
    mkdir(outDir);
end

% Make output subfolder, checking if folder exists and acting accordingly 
% with override options
outSubDir = strcat(outDir, '/', outName);
outSubDir = checkIfExistsOrIncrement(outSubDir, 'dir', conf.override); 
% outSubDir = char(outSubDir);

if ~exist(outSubDir,'dir')
    mkdir(outSubDir);
end

% Make plotdir 
plotDir = strcat(outSubDir, '/images');
if ~exist(plotDir,'dir')
    mkdir(plotDir);
end

% Make DynPlotDir 
DynPlotDir = strcat(plotDir, '/DynZspectra');
if ~exist(DynPlotDir,'dir')
    mkdir(DynPlotDir);
end

% Make DynPlotDir2 
DynPlotDir2 = strcat(plotDir, '/DynZspectraDiff');
if ~exist(DynPlotDir2,'dir')
    mkdir(DynPlotDir2);
end

% Make textdir 
textDir = strcat(outSubDir, '/text');
if ~exist(textDir,'dir')
    mkdir(textDir);
end

end

