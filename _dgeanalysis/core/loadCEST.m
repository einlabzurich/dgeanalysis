function [dynamic_scan, initial_z_spectrum, final_z_spectrum, anatomical_scan] = loadCEST(i,exps)

dir_CEST = exps{1,i}.workdir;
matName  = exps{1,i}.shortName;

%% Read anatomical scan
% Import needed info from the files 'read_acqp', 'read_method', 'read_reco' and 'read_2dseq'
[parameters_acqp]   = read_acqp(exps{1,i}.anatomical_scan_folder_number, dir_CEST);
[parameters_method] = read_method(exps{1,i}.anatomical_scan_folder_number, dir_CEST);
[parameters_reco]   = read_reco(exps{1,i}.anatomical_scan_folder_number, dir_CEST);     
[parameters_2dseq]  = read_2dseq(exps{1,i}.anatomical_scan_folder_number,...
                               dir_CEST, parameters_reco, parameters_acqp);

% Saves all parameters from the four functions in 'anatomical_scan' structure
[anatomical_scan] = structcat(parameters_acqp, parameters_method, ...
                              parameters_reco, parameters_2dseq);
% Remove some variables since they are stored in 'anatomical_scan' struct
clear anatomical_scan_folder_number            
clear parameters_acqp   parameters_method   parameters_reco   parameters_2dseq 

%% Read dynamic scan
   [parameters_acqp]   = read_acqp (exps{1,i}.dynamic_scan,dir_CEST);
   [parameters_method] = read_method(exps{1,i}.dynamic_scan, dir_CEST);
   [parameters_reco]   = read_reco(exps{1,i}.dynamic_scan, dir_CEST);                  
   [parameters_2dseq]  = read_2dseq(exps{1,i}.dynamic_scan, dir_CEST,...
                                  parameters_reco, parameters_acqp);
   % Saves all parameters from the four functions in 'cest_scan' structure.
   [dynamic_scan] = structcat(parameters_acqp, parameters_method, ...
                               parameters_reco, parameters_2dseq);
% Remove some variables since they are stored in 'dynamic_scan' struct
clear parameters_acqp   parameters_method   parameters_reco   parameters_2dseq 

%% Read initial z-spectrum scan
   [parameters_acqp]   = read_acqp (exps{1,i}.initial_z_spectrum,dir_CEST);
   [parameters_method] = read_method(exps{1,i}.initial_z_spectrum, dir_CEST);
   [parameters_reco]   = read_reco(exps{1,i}.initial_z_spectrum, dir_CEST);                  
   [parameters_2dseq]  = read_2dseq(exps{1,i}.initial_z_spectrum, dir_CEST,...
                                  parameters_reco, parameters_acqp);
   % Saves all parameters from the four functions in 'cest_scan' structure.
   [initial_z_spectrum] = structcat(parameters_acqp, parameters_method, ...
                               parameters_reco, parameters_2dseq);
% Remove some variables since they are stored in 'initial_z_spectrum' struct
clear parameters_acqp   parameters_method   parameters_reco   parameters_2dseq                            

%% Read final z-spectrum scan
   [parameters_acqp]   = read_acqp (exps{1,i}.final_z_spectrum,dir_CEST);
   [parameters_method] = read_method(exps{1,i}.final_z_spectrum, dir_CEST);
   [parameters_reco]   = read_reco(exps{1,i}.final_z_spectrum, dir_CEST);                  
   [parameters_2dseq]  = read_2dseq(exps{1,i}.final_z_spectrum, dir_CEST,...
                                  parameters_reco, parameters_acqp);
   % Saves all parameters from the four functions in 'cest_scan' structure.
   [final_z_spectrum] = structcat(parameters_acqp, parameters_method, ...
                               parameters_reco, parameters_2dseq);
% Remove some variables since they are stored in 'final_z_spectrum' struct
clear parameters_acqp   parameters_method   parameters_reco   parameters_2dseq 

outputFile = char(fullfile(dir_CEST, matName));
save(outputFile, 'dynamic_scan', 'initial_z_spectrum', 'final_z_spectrum', 'anatomical_scan')

end
