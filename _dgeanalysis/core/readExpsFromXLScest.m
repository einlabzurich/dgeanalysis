function [exps] = readExpsFromXLScest(xlsFile, inFilesBasePath)

scoresheet = readtable(xlsFile); 
nexp = size(scoresheet,1); 

if nexp == 0
    error('No experiments in dataset!')
end

i = 0;
for e = 1:nexp
    i = i+1;
    
    exclude = ~logical(scoresheet{e,1});
    
    if exclude == 1
        i = i - 1;
        continue;
    end

    path = scoresheet{e,2}{1};
    
    % Next try/catch are for catching NaNs
    try
        subpath1 = scoresheet{e,3}{1};
    catch 
        subpath1 = '';
    end
    
    try
        subpath2 = scoresheet{e,4}{1};
    catch 
        subpath2 = '';
    end
        
    try
        subpath3 = scoresheet{e,5}{1};
    catch 
        subpath3 = '';
    end
    
    path = [path, '/', subpath1, '/', subpath2, '/', subpath3];    
    path = strrep(path, '///', '/');
    path = strrep(path, '//', '/');
    exps{i}.workdir = fullfile(inFilesBasePath, path);

    %Study specific parameters
    exps{i}.anatomical_scan_folder_number = scoresheet{e,6};
    exps{i}.initial_z_spectrum            = scoresheet{e,7};
    exps{i}.dynamic_scan                  = scoresheet{e,8};
    exps{i}.final_z_spectrum              = scoresheet{e,9};
    exps{i}.shortName                     = scoresheet{e,10}{1};
    
    file = dir(fullfile(exps{i}.workdir,'cortexROI','*.tif'));
    if isempty(file)
        exps{i}.cortexROIExists = 0;
        exps{i}.cortexROIFile = '';
        exps{i}.cortexROIName = '';
    else
        exps{i}.cortexROIExists = 1;
        exps{i}.cortexROIFile = fullfile(exps{i}.workdir,'cortexROI',file.name); 
        exps{i}.cortexROIName = strcat('cortexROI_', exps{i}.shortName);        
    end
    
    file1 = dir(fullfile(exps{i}.workdir,'brainMask','*.tif'));
    if isempty(file1)
        exps{i}.brainMaskExists = 0;
        exps{i}.brainMaskFile = '';
        exps{i}.brainMaskName = '';
    else
        exps{i}.brainMaskExists = 1;
        exps{i}.brainMaskFile = fullfile(exps{i}.workdir,'brainMask',file1.name); 
        exps{i}.brainMaskName = strcat('brainMask_', exps{i}.shortName);        
    end
      
end
    
end

