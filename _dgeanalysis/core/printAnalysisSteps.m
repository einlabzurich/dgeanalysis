function [] = printAnalysisSteps(RGB, image, ref, Cmap, outName, varargin)

persistent p;
if isempty(p)
    p = inputParser;
    p.FunctionName = 'printAnalysisSteps';
    p.addRequired('RGB');
    p.addRequired('image');
    p.addRequired('ref');
    p.addRequired('Cmap');
    p.addRequired('outName');
    p.addParameter('cmapMin', -1); % For some reason it complains if I put min(image(:)) here.
    p.addParameter('cmapMax', -1);
end

parse(p, RGB, image, ref, Cmap, outName, varargin{:});

cmapMin = p.Results.cmapMin;
cmapMax = p.Results.cmapMax;

if cmapMin == -1
    cmapMin = min(image(~isinf(image(:)) & ~isnan(image(:))));
end
if cmapMax == -1
    cmapMax = max(image(~isinf(image(:)) & ~isnan(image(:))));
end

% Printing
fig1 = figure('pos',[100 100 1200 275], 'visible', 'off');

subplot(1,3,1)
imagesc(ref./max(ref(:)))
title('Intensity')
colormap(gca, gray(256));
axis off;
colorbar;

subplot(1,3,2)
imagesc(image)
title('Ratio')
colormap(gca, Cmap);
axis off;
colorbar;
caxis([cmapMin, cmapMax]);

subplot(1,3,3)
imagesc(RGB)
title('Weighted ratio')
colormap(gca, Cmap);
axis off;
colorbar;
caxis([cmapMin, cmapMax]);

figName = strcat(outName, '_mergeSteps.png');
saveas(fig1, figName);


end

