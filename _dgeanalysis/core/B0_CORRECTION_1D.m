function [Mz_corr,Mz_corrInterp]=B0_CORRECTION_1D(image,dB0,P)
try
    B0_int_meth=P.EVAL.B0_int_meth;
catch
    B0_int_meth='linspline'; % i.e. linspline, linear, spline,cubic,pchip
end  
w_interp  = P.EVAL.w_interp;
x_offsets = P.SEQ.w;

x_interp = P.EVAL.w_fit;
try
    if strcmp(B0_int_meth,'linspline')
        Mz_corr = (interp1(x_offsets-dB0,image,w_interp,'linear','extrap')+...
                   interp1(x_offsets-dB0,image,w_interp,'spline','extrap'))/2;
    else
        Mz_corr       = interp1(x_offsets-dB0,image,w_interp,B0_int_meth,'extrap');
        %interpolated points, spline interpolation A.E. 
        Mz_corrInterp = interp1(x_offsets-dB0,image,x_interp,B0_int_meth,'extrap');
    end    
catch MS
    warning('B0 correction failed during interpolation');
    rethrow(MS)
end