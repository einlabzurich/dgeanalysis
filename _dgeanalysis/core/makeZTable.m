function [Tables] = makeZTable(data, ppms)

colNames = {'offsets', 'DynZspec'};
Matrix   = [ppms', data];

% Make the table
Tables = array2table(Matrix, 'VariableNames', colNames);

end

