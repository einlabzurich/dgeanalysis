function [Tables] = STAsymTable(I1,I2, timeScale, nBL)

warning('off');    

diff           = I2-I1;
ratio          = I2./I1;
diffI2I1NormI2 = (I2-I1)./I2;

I1nBL    = I1   ./ mean(I1(nBL));
I2nBL    = I2   ./ mean(I2(nBL));
diffnBL  = diff ./ mean(diff(nBL));
rationBL = ratio./ mean(ratio(nBL));

diffI2I1NormI2nBL = diffI2I1NormI2 ./mean(diffI2I1NormI2(nBL));

colNames = {'time'    ,'I1', 'I2', 'I1neg','diffI2I1NormI2','diffI2I1','ratioI2I1','I1nBL','I2nBL','I1negnBL','diffI2I1NormI2nBL','diffI2I1nBL','ratioI2I1nBL'};
Matrix   = [timeScale', I1,   I2,   -1*I1,  diffI2I1NormI2,  diff,      ratio,      I1nBL,  I2nBL,  -I1nBL,  diffI2I1NormI2nBL,  diffnBL,  rationBL ];

% Make the table
Tables = array2table(Matrix, 'VariableNames', colNames);

end

