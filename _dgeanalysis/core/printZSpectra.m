function [] = printZSpectra(plotDir, ZPreTable,ZPostTable, shortName)

x = ZPreTable.offsets;
x_label = 'Offset / ppm';

% Make the figure    
figName = strcat(plotDir, '/', shortName, '_ZSpectra.jpg');
fig = figure('pos',[100 100 1024 512], 'visible', 'off');

Xmax = max(x);
Xmin = min(x);

% without B0 correction (Zpre and Zpost)
subplot(1,2,1)  

plot(x, ZPreTable.noB0corr, '-k');
hold on
plot(x, ZPostTable.noB0corr, '-r');

Ymax = max(ZPreTable.noB0corr)*1.1;
Ymin = min(ZPreTable.noB0corr)*0.9;
xlim([Xmin, Xmax]);
ylim(sort([Ymin, Ymax])); % Sort is needed to avoid crash when numerical instabilities give negative numbers

title('no B0 correction')

xlabel(x_label);
ylabel(' Water signal / a.u.');

% Weighted Mean tau
subplot(1,2,2)  

plot(x, ZPreTable.B0corr, '-k');
hold on
plot(x, ZPostTable.B0corr, '-r');

Ymax = max(ZPreTable.B0corr)*1.1;
Ymin = min(ZPreTable.B0corr)*0.9;
xlim([Xmin, Xmax]);
ylim(sort([Ymin, Ymax])); % Sort is needed to avoid crash when numerical instabilities give negative numbers

title('B0 correction')

xlabel(x_label);

% Save the figure
saveas(fig, figName);
    
end


