function [ZspecPre, ZspecPost, ppmVals] = sortChemicalShifts(final_ppm_values,initial_ppm_values,initial_z_spectrum,final_z_spectrum)
if any(round(final_ppm_values-initial_ppm_values,4))
        error('Z spectra have different offsets!');
end
    [ppmVals, ppmIdx] = sort(initial_ppm_values); %sorts the order of the ppm values
    ZspecPre          = initial_z_spectrum(:,:,1,ppmIdx);
    ZspecPost         = final_z_spectrum(:,:,1,ppmIdx);
end