CONFIGURATION INFORMATION
=========================

Path         : /opt/PV6.0/conf/instr/spect/uxnmr.info
Date         : Wed Nov 19 13:06:07 2014
Release      : TopSpin Acquisition Version 3.1PV beta-pl0
Installed in : /opt/PV6.0
Host         : CZC412577F
OS           : CentOS release 5.10 (Final)
CPU          : Intel(R) Xeon(R) CPU E5-1620 v2 @ 3.70GHz (8 cores at 1200 MHz with Hyperthreading)
User         : nmrsu (NMR Administrator (nmrsu))
Description  : BAP70/30
Location     : Institut fuer Pharmakologie und Toxikologie
System       : Avance III NMR spectrometer
1H-frequency : 300.34 MHz
Order Number : 10151720
Configured in: /opt/PV6.0/conf/instr/spect

IPSO: connected to spectrometer subnet
- TCP/IP address = 149.236.99.246
 - IPSO type: AQS
- Tctrl : 1
- Fctrls: 2
- Gctrl1: with digital preemphasis
- Rctrl : 1
- FREDs : 1
- DPP : 1

DRU: AQS DRU-E Z102520/03964 ECL 07.00
- TCP/IP address = 149.236.99.89
- Firmware Version = 131205
- DRU controls AQS-Rack and HPPR/2
DRU: AQS DRU-E Z102520/03963 ECL 07.00
- TCP/IP address = 149.236.99.88
- Firmware Version = 131205
DRU: AQS DRU-E Z102520/03966 ECL 07.00
- TCP/IP address = 149.236.99.87
- Firmware Version = 131205
- DRU controls AQS-Rack and HPPR/2
DRU: AQS DRU-E Z102520/03965 ECL 07.00
- TCP/IP address = 149.236.99.86
- Firmware Version = 131205

AQS: connected to 149.236.99.89:/dev/tty10
  _Slot_ SBSB _____________________Board_____________________
  Number Addr Type HW-VS FW-VS ID  ECL Name   Description
  -----------------------------------------------------------
     0   0x24 0x32     0        S  2.2 SGU-1  AQS SGU/3 600 (2CH) Z117129/01635 ECL 02.02 FPGA-Vs=20131031
     2   0x10 0x43   0x5    CD  R  0.2 REC-1  AQS RXAD/2 600 Z130588/1296 ECL 00.02
     3   0x34 0xd6   0x1        X  3.0 REF-1  REF/3-600 Reference Board for AQS/3 Receiver
     6   0x11 0x43   0x5    CD  R  0.2 REC-2  AQS RXAD/2 600 Z130588/1293 ECL 00.02
    10   0x1d 0xcb     0    --  N  1.0 PUL-1  AQS PULSE SPLITTER Z104431/00233 ECL 01.00
    13   0xa0 0x93     0        V  0.1 PS-1   
    14   0xa1 0x91     0        V  1.2 PS-2   AQS PSM-D Power Supply Module
    --   0x20 0xd8     0        B  1.1 MASTER AQS/3+ Chassis
     1   0x20  0x7     0        B      MASTER AQS Rack Master

AQS2M-1: connected to 149.236.99.87:/dev/tty10
  _Slot_ SBSB _____________________Board_____________________
  Number Addr Type HW-VS FW-VS ID  ECL Name   Description
  -----------------------------------------------------------
     2   0x10 0x43   0x5    CD  R  0.2 REC-3  AQS RXAD/2 600 Z130588/1294 ECL 00.02
     4   0x11 0x43   0x5    CD  R  0.2 REC-4  AQS RXAD/2 600 Z130588/1295 ECL 00.02
    16   0x1c 0xca   0x1    --  M  2.1 RFS-1  AQS RF SPLITTER Z104432/00244 ECL 02.01
    --   0x20 0xce     0        B  3.2 AQS/2-M AQS/2M Chassis
     1   0x20  0x7     0        B      MASTER AQS Rack Master


Router: none installed


Transmitters at the spectrometer subnet:
----------------------------------------
BLA_W1345501_0346 W1345501/0346 ECL 21:
- TCP/IP address = 149.236.99.245
- Firmware VS    = 20120627
- Amplifier      = BLA1000 IE 15-400MHZ: W1345501/0346 ECL 21
- Controller     = BLA CONTROL BOARD 6 2CH: W1522050/7908 ECL 50
BLA_W1345501_0347 W1345501/0347 ECL 21:
- TCP/IP address = 149.236.99.244
- Firmware VS    = 20120627
- Amplifier      = BLA1000 IE 15-400MHZ: W1345501/0347 ECL 21
- Controller     = BLA CONTROL BOARD 6 2CH: W1522050/7960 ECL 50

Gradient amplifiers at the spectrometer subnet:
----------------------------------------------
BGA1: BGAU_W134655_0044
- TCP/IP address   = 149.236.99.242
- Firmware version = 20140120
- Web version      = 6.0
- Current limits   = 0.0/X, 0.0/Y, 0.0/Z (in A)

Preamplifiers :
HPPR: - HPPR/2 preamplifier connected to 149.236.99.89:/dev/tty10
    Type      : HPPR/2
    Controller: Application firmware = c.
                no LED display for tuning and matching
    Module  1 : HPLNA 19F1H (virtual 50 Ohm reference: 99.9%/0.1deg, reflection meter)
                 PN=Z103202, SN=00170 from 20130927
    Module  2 : HPLNA BB31P (reflection meter)
                 PN=Z111088, SN=00081 from 20131203
    Module  3 : HPLNA 19F1H (virtual 50 Ohm reference: 99.9%/0.1deg, reflection meter)
                 PN=Z103202, SN=00302 from 20140129
    Module  4 : ADM Function Module



Frequency generation:
- F1: for SGU
- F2: for SGU



RF cable connections (detected by 'confamp')
--------------------------------------------------------------------
SGU1 NORM output -> input 1 of transmitter 1 (BLA1000 IE 15-400MHZ W1345501/0346 at TCP/IP 149.236.99.245)
SGU1 AUX  output -> TUNE signal input of HPPR
SGU2 NORM output -> input 1 of transmitter 2 (BLA1000 IE 15-400MHZ W1345501/0347 at TCP/IP 149.236.99.244)
SGU2 AUX  output -> TUNE signal input of HPPR

Blanking cable connections (detected by 'confamp')
--------------------------------------------------------------------
transmitter 1 (BLA1000 IE 15-400MHZ W1345501/0346 at TCP/IP 149.236.99.245) amplifier B-1000W uses blanking 1
transmitter 1 (BLA1000 IE 15-400MHZ W1345501/0346 at TCP/IP 149.236.99.245) amplifier B-300W uses blanking 1
transmitter 2 (BLA1000 IE 15-400MHZ W1345501/0347 at TCP/IP 149.236.99.244) amplifier B-1000W uses blanking 2
transmitter 2 (BLA1000 IE 15-400MHZ W1345501/0347 at TCP/IP 149.236.99.244) amplifier B-300W uses blanking 2

LO cable connections (detected by 'confrecsel')
--------------------------------------------------------------------
SGU1 -> Rf-Splitter input 1 (-> REC1)
SGU1 -> SGU2 -> Rf-Splitter input 2 (-> REC1-4)

Preamplifier connections (detected by 'confrecpre')
--------------------------------------------------------------------
Tune-SGU2 -> HPLNA 19F1H -> REC1
Tune-SGU2 -> HPLNA BB31P -> REC1
Tune-SGU1 -> HPLNA 19F1H -> REC2

