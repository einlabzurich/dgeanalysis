# DGEanalysis

DGEanalysis is a small Matlab program to analyse Chemical Exchange Saturation Transfer (CEST) experiments. 
It is partially based on the code developed by Moritz Zaiss (https://www.cest-sources.org/doku.php) and partially on simple functions

## Documentation

The documentation of the DGEanalysis code can be found in the doc directory.
