function [conf] = dgeanalysisConfig()

%% Input/output options %%
conf.CESTPath = 'F:\Afroditi\MatlabCode\dgeanalysis\_dgeanalysis'; % Path to the main CEST analysis folder (the one containing the core code)
conf.tableFileName = 'F:\Afroditi\MatlabCode\dgeanalysis\User\TestData.xlsx'; % Full path of the excel file containing the info about the experiments
conf.inFilesBasePath = 'F:\Afroditi\MatlabCode'; % Base common path for the files listed in the table file
conf.outName = 'Test'; % Name of the current analysis (i.e. name of the subdir in Results)
conf.includeOutNameInFilenames = 1; %SWITCH (0/1): Set to 1 to have conf.outName appended to the name of all output files
conf.override = 1; %SWITCH (0/1): Set to 1 to override analysis dirs with the same name (otherwise add progressive numbering)
conf.disableWarnings = 0; %SWITCH (0/1): Set to 1 to disable warnings in the terminal

%% Analysis options
conf.meanOfAllPixels = 0; %SWITCH (0/1): sum all the pixels of the defined ROI together to have higher SNR curves 

%Brain and Cortex ROI
conf.doBrainMask = 1; %SWITCH (0/1): segment image to keep only the whole brain (NaN everything else)
conf.doCortexROI = 0; %SWITCH (0/1): segment image to keep only the cortex (NaN everything else)

% motion correction
conf.motionCorr = 1; %SWITCH (0/1): do motion correction
conf.MaxIterations = 2000; % used in motion correction and in registering the post Z-spectrum with respect to the pre Z-spectrum
conf.alignZSpecPost = 1; %SWITCH (0/1): motion correction of Post to Pre

% Time smoothing
conf.timeSmooth = 1; %SWITCH (0/1): do time smoothing
conf.timeSmoothHalfWindow = 20; % half window of moving average
conf.tSMAmethod = 'gaussian'; %options: 'movmean','movmedian','gaussian','lowess','loess','rlowess','rloess','sgolay'

%Calculating the B0 map 
conf.dynamicScanB0map = 1; %SWITCH (0/1): calculate the B0 maps of each time point (dynamic B0 map)
conf.step = 0.0005; %step of interpolation of z-spectra
conf.doParallel = 1; %SWITCH (0/1): do parallel programming in finding the min of Z-specta
conf.splinesmoothing = 0.99; % values between 0 and 1.
conf.B0InterpMeth = 'spline'; % options: 'linspline', 'linear', 'spline', 'cubic', 'pchip'

% B0 correction
conf.doB0Corr = 1; %SWITCH (0/1): B0 correction on dynamic scan
conf.B0CorrOpt = 'dynZscan'; % options: 'fullZspec', 'dynZscan'
conf.keepUncorrZ = 1; %SWITCH (0/1): keep uncorrected Z-spectra in memory (only for debugging- takes time and space)
conf.interpB0PrePost = 1; %SWITCH (0/1): 1: B0 correction using interpolation between pre and post Z-spectrum; 0: use only the pre Z-spectrum

%Dynamic Glucose Enhancement (DGE) Contrast
conf.BaselineImgs = 1:15; % baseline images to be used for normalization and analysis; keep format 
conf.InvestigatedCS = 1.2; % the ppm value to be investigated, for glucose 1.2 (ppm)
conf.doSTAsym = 1; %SWITCH (0/1): calculate saturation transfer assymetry curve

%images and plots
conf.printDynZspectra = 0; %SWITCH (0/1): print dynamic Z-spectra
conf.printOnlyInvestigatedCS = 0; %SWITCH (0/1): print only investigated chemical shift curves
conf.printDGEimages = 1; %SWITCH (0/1): print DGE images

conf.RGBimgs.intWeight = 1; % SWITCH (0/1): print intensity weighted RGB/TIFFs
conf.RGBimgs.brightScaleFactor = 1.2; % Scales the intensity of the RGB weighted image (does not change colors)
conf.RGBimgs.colormap = 'roycb'; % See colorMapsDoc file for list of available colormaps
conf.RGBimgs.cmDepth = 256; % Numer of colors nuances in the colormap
conf.RGBimgs.highlightSaturation = 0; % Color white all pixels above Imax
conf.RGBimgs.printCB = 1; % SWITCH (0/1): print the colorbar as separate image
conf.RGBimgs.printSteps = 1; % SWITCH (0/1): print a Matlab Figure with all steps of intensity weighting

conf.RGBimgs.I2I1.cmapMin = 0.5; % Minimum value for the colormap (use -1 to autodetect minimum)
conf.RGBimgs.I2I1.cmapMax = 1.0; % Maximum value for the colormap (use -1 to autodetect maximum)
conf.RGBimgs.I1nBL.cmapMin = 0.9; % Minimum value for the colormap (use -1 to autodetect minimum)
conf.RGBimgs.I1nBL.cmapMax = 1.1; % Maximum value for the colormap (use -1 to autodetect maximum)

%% ANYTHING BELOW THIS LINE IS FOR INTERNAL WORKING OF THE CODE. DO NOT CHANGE!            
conf.configPath = strcat(mfilename('fullpath'), '.m');

end