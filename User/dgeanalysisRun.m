%% version Afroditi Eleftheriou, Luca Ravotto 21.11.2022

%Import the configuration options
conf = dgeanalysisConfig();

% Add necessary paths
addpath(conf.CESTPath);
addpath(strcat(conf.CESTPath,'/core'));

% Run CESTanalysis
fDGEanalysis(conf);
